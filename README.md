[TOC]
 
# DHCP Configuration
In dieser Dokumentation wird erklärt, wie man auf Filius einen **DHCP-Server** aufsetzen und konfiguriert.
 
## DHCP Setup
Bei der Ausgangslage handelt es sich um eine
**Sterne-Architektur** mit einem **DHCP-Server** und **drei CLients**
 
![Architektur](./IMAGES/1.png)
 
### Konfigurationsschritte
-DHCP Server icon anklicken
-Staatische-IP für Client 3 setzen
-IP-Range zuweisen
-"DHCP Server einrichten" anwählen dann auf OK

![Architektur](./IMAGES/3.png)
![Architektur](./IMAGES/4.png)

### DHCP Server einrichten
-Bei allen Clients "DHCP zur Konfiguration verwenden" auswählen

![DHCP-Server](./IMAGES/2.png)